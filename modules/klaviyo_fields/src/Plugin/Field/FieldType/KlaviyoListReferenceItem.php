<?php

namespace Drupal\klaviyo_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'klaviyo_list_reference' entity field type.
 *
 * @FieldType(
 *   id = "klaviyo_list_reference",
 *   label = @Translation("Klaviyo list"),
 *   description = @Translation("A field containing the ID of a remote Klaviyo list."),
 *   category = @Translation("Reference"),
 *   default_widget = "klaviyo_list_select",
 *   default_formatter = "klaviyo_list_name"
 * )
 */
class KlaviyoListReferenceItem extends StringItem {

  /**
   * The machine name of the field setting that controls the API key to use.
   *
   * This config value can be NULL if the system-default API key should be used.
   */
  public const FIELD_CONFIG_API_KEY = 'klaviyo_api_key';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    $storage_settings = parent::defaultStorageSettings();

    // The is_ascii option doesn't make sense for list IDs.
    unset($storage_settings['is_ascii']);

    // List IDs appear to be 6 digits. Let's double that for future-proofing.
    $storage_settings['max_length'] = 12;

    return $storage_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
      self::FIELD_CONFIG_API_KEY => NULL,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form,
                                    FormStateInterface $form_state): array {
    $form = parent::fieldSettingsForm($form, $form_state);

    $form[self::FIELD_CONFIG_API_KEY] = [
      '#type'          => 'key_select',
      '#title'         => $this->t('Klaviyo API key'),
      '#description'   => $this->t('The API key to use when connecting to Klaviyo to enumerate what lists are available.'),
      '#empty_value'   => NULL,
      '#empty_option'  => $this->t('- System default -'),
      '#key_filters'   => ['type' => 'klaviyo_api'],
      '#required'      => FALSE,
      '#default_value' => $this->getSetting(self::FIELD_CONFIG_API_KEY),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(
                    FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type'   => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
          'binary' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form,
                                      FormStateInterface $form_state,
                                      $has_data): array {
    return [];
  }

}

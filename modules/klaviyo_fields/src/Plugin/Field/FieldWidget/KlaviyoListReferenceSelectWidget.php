<?php

namespace Drupal\klaviyo_fields\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\klaviyo_fields\Plugin\Field\FieldSettingsTrait;

/**
 * Plugin implementation of the 'klaviyo_list_select' widget.
 *
 * @FieldWidget(
 *   id = "klaviyo_list_select",
 *   label = @Translation("Klaviyo list selector"),
 *   field_types = {
 *     "klaviyo_list_reference"
 *   },
 *   multiple_values = TRUE
 * )
 *
 * @noinspection PhpUnused
 */
class KlaviyoListReferenceSelectWidget extends OptionsWidgetBase {

  use FieldSettingsTrait;

  /**
   * Whether a value for this field is required.
   *
   * BUGBUG: This property is dynamically defined in the parent class.
   *
   * @var bool
   */
  protected $required;

  /**
   * Whether this field accepts multiple values.
   *
   * BUGBUG: This property is dynamically defined in the parent class.
   *
   * @var bool
   */
  protected $multiple;

  // phpcs:disable Drupal.NamingConventions.ValidVariableName.LowerCamelName
  /**
   * Whether this field has been populated with a value.
   *
   * BUGBUG: This property is dynamically defined in the parent class.
   *
   * @var bool
   */
  protected $has_value;
  // phpcs:enable Drupal.NamingConventions.ValidVariableName.LowerCamelName

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items,
                              $delta,
                              array $element,
                              array &$form,
                              FormStateInterface $form_state): array {
    $element =
      parent::formElement($items, $delta, $element, $form, $form_state);

    $element += [
      '#type'          => 'klaviyo_list_select',
      '#api_key_id'    => $this->getConfiguredApiKeyId(),
      '#default_value' => $this->getSelectedOptions($items),
      '#multiple'      => $this->multiple,
      '#empty_value'   => NULL,
      '#empty_option'  => $this->getEmptyLabel(),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSelectedOptions(FieldItemListInterface $items): array {
    return array_map(
      function (FieldItemInterface $item) {
        return $item->{$this->column};
      },
      iterator_to_array($items)
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function sanitizeLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    $label = '';

    if ($this->required) {
      if (!$this->multiple && !$this->has_value) {
        $label = t('- Select a value -');
      }
    }
    else {
      $label = t('- None -');
    }

    return $label;
  }

}

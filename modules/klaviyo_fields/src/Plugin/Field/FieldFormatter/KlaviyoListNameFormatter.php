<?php

namespace Drupal\klaviyo_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\klaviyo_api\Element\KlaviyoOptionsListTrait;
use Drupal\klaviyo_fields\Plugin\Field\FieldSettingsTrait;

/**
 * Plugin implementation of the 'text_default' formatter.
 *
 * @FieldFormatter(
 *   id = "klaviyo_list_name",
 *   label = @Translation("Klaviyo list name"),
 *   field_types = {
 *     "klaviyo_list_reference",
 *   }
 * )
 *
 * @noinspection PhpUnused
 */
class KlaviyoListNameFormatter extends FormatterBase {

  use FieldSettingsTrait;
  use KlaviyoOptionsListTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items,
                               $langcode): array {
    $elements = [];

    $api_key_id      = $this->getConfiguredApiKeyId();
    $list_repository = self::getListRepository($api_key_id);
    $options         = self::fetchOptions($list_repository);

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewElement($options, $item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one item of the field.
   *
   * @param string[] $options
   *   An associative array of Klaviyo lists, where the value is the
   *   human-friendly name for the list and the key is the ID of the list in
   *   Klaviyo.
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item to render.
   *
   * @return array
   *   A render array to generate output that includes the value of the item.
   *   The textual output generated as a render array.
   */
  protected function viewElement(array $options,
                                 FieldItemInterface $item): array {
    try {
      $value = $item->get('value')->getValue();
    }
    catch (MissingDataException $ex) {
      throw new \RuntimeException(
        'Failed to load stored list value: ' . $ex->getMessage(),
        0,
        $ex
      );
    }
    $label = $options[$value];

    return [
      '#type'     => 'inline_template',
      '#template' => '{{ value|nl2br }}',
      '#context'  => ['value' => $label],
    ];
  }

}

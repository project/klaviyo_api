<?php

namespace Drupal\klaviyo_fields\Plugin\Field;

use Drupal\klaviyo_fields\Plugin\Field\FieldType\KlaviyoListReferenceItem;

/**
 * A trait for field widgets and formatters to retrieve common field settings.
 */
trait FieldSettingsTrait {

  /**
   * The field definition to which this widget or formatter corresponds.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * Gets the machine name of the Klaviyo API key to use for retrieving values.
   *
   * Can be NULL if the system-default key should be used.
   *
   * @return string|null
   *   Either the machine name of the API key to use for this field; or NULL to
   *   use the system-default API key.
   */
  public function getConfiguredApiKeyId(): ?string {
    return $this
      ->fieldDefinition
      ->getSetting(KlaviyoListReferenceItem::FIELD_CONFIG_API_KEY);
  }

}

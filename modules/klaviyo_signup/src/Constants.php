<?php

namespace Drupal\klaviyo_signup;

/**
 * A class containing all constants used in multiple places of this module.
 *
 * Where possible, constants specific to an interface or specific classes should
 * live in those interfaces and classes instead of this class.
 */
abstract class Constants {

  /**
   * The machine name for this module.
   */
  const MODULE_NAME = 'klaviyo_signup';

  /**
   * The machine name of the config for this module.
   */
  const MODULE_CONFIG_ID = 'klaviyo_signup.settings';

  /**
   * Name of the key in module config holding which list new users get added to.
   */
  const CONFIG_KEY_KLAVIYO_SIGNUP_LIST_ID = 'signup_list_id';

}

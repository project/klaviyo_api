<?php

/**
 * @file
 * Hooks provided by the Klaviyo API module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\user\UserInterface;

/**
 * Alter the profile details of a Klaviyo user profile being created or updated.
 *
 * @param array &$profile_data
 *   A reference to the array of profile data being sent to Klaviyo. Typically
 *   includes:
 *     - '$email': Email address.
 *     - '$first_name': First name.
 *     - '$last_name': Last name.
 *     - '$organization': Company, business, or organization.
 *     - '$title': Title at company, business, or organization.
 *     - '$address1': First address line.
 *     - '$address2': Second address line.
 *     - '$city': City.
 *     - '$region': Region or state.
 *     - '$zip': Postal code.
 *     - '$country': Country name.
 *     - '$timezone': Time zone.
 *     - '$phone_number': Phone number.
 * @param \Drupal\user\UserInterface $user
 *   The user for which the profile is being assembled.
 *
 * @see https://github.com/klaviyo/php-klaviyo/blob/master/src/Model/ProfileModel.php
 *
 * @ingroup klaviyo_api
 */
function hook_klaviyo_user_profile_alter(array &$profile_data,
                                         UserInterface $user) {
  // Force all users to have the name "John Smith" in Klaviyo.
  // Take that, marketers!
  $profile_data['$first_name'] = 'John';
  $profile_data['$last_name']  = 'Smith';
}

/**
 * @} End of "addtogroup hooks".
 */

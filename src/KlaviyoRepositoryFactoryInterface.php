<?php

namespace Drupal\klaviyo_api;

use Drupal\key\KeyInterface;

/**
 * An interface for obtaining repository services for custom API keys.
 *
 * Before using this interface, consider if you will just be using the
 * site-wide default API key. If you are, you don't need to use this interface.
 * Instead, just inject/rely on the default repository services (e.g.
 * "klaviyo_api.list.repository", "@klaviyo_api.client", etc.) instead to save
 * yourself from having to work with keys directly.
 */
interface KlaviyoRepositoryFactoryInterface {

  /**
   * Obtains a list repository that connects to Klaviyo with provided key.
   *
   * @param string $key_id
   *   The machine name of the Key entity containing the credentials to connect
   *   to Klaviyo. Must be a Key of type "klaviyo_api".
   *
   * @return \Drupal\klaviyo_api\KlaviyoListRepositoryInterface
   *   The repository for interacting with lists in the remote Klaviyo instance
   *   that is reachable with the provided API key.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildListRepositoryFromKeyId(
                              string $key_id): KlaviyoListRepositoryInterface;

  /**
   * Obtains a list repository that connects to Klaviyo with provided key.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The API key the client should use for connecting. The key must be of
   *   type "klaviyo_api".
   *
   * @return \Drupal\klaviyo_api\KlaviyoListRepositoryInterface
   *   The repository for interacting with lists in the remote Klaviyo instance
   *   that is reachable with the provided API key.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildListRepositoryFromKey(
                            KeyInterface $key): KlaviyoListRepositoryInterface;

  /**
   * Obtains a profile repository that connects to Klaviyo with provided key.
   *
   * @param string $key_id
   *   The machine name of the Key entity containing the credentials to connect
   *   to Klaviyo. Must be a Key of type "klaviyo_api".
   *
   * @return \Drupal\klaviyo_api\KlaviyoProfileRepositoryInterface
   *   The repository for interacting with user profiles in the remote Klaviyo
   *   instance that is reachable with the provided API key.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildProfileRepositoryFromKeyId(
                            string $key_id): KlaviyoProfileRepositoryInterface;

  /**
   * Obtains a profile repository that connects to Klaviyo with provided key.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The API key the client should use for connecting. The key must be of
   *   type "klaviyo_api".
   *
   * @return \Drupal\klaviyo_api\KlaviyoProfileRepositoryInterface
   *   The repository for interacting with user profiles in the remote Klaviyo
   *   instance that is reachable with the provided API key.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildProfileRepositoryFromKey(
                        KeyInterface $key): KlaviyoProfileRepositoryInterface;

}

<?php

namespace Drupal\klaviyo_api;

use Drupal\key\KeyInterface;
use Klaviyo\Klaviyo;

/**
 * The default service for instantiating repository instances from custom keys.
 */
class KlaviyoRepositoryFactory implements KlaviyoRepositoryFactoryInterface {

  /**
   * The factory for Klaviyo API clients.
   *
   * @var \Drupal\klaviyo_api\KlaviyoClientFactoryInterface
   */
  protected $apiFactory;

  /**
   * Constructor for KlaviyoRepositoryFactory.
   *
   * @param \Drupal\klaviyo_api\KlaviyoClientFactoryInterface $api_factory
   *   The Klaviyo API client.
   */
  public function __construct(KlaviyoClientFactoryInterface $api_factory) {
    $this->apiFactory = $api_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRepositoryFromKeyId(
                              string $key_id): KlaviyoListRepositoryInterface {
    $api_client = $this->buildClientFromKeyId($key_id);

    return new KlaviyoListRepository($api_client);
  }

  /**
   * {@inheritdoc}
   */
  public function buildListRepositoryFromKey(
                            KeyInterface $key): KlaviyoListRepositoryInterface {
    $api_client = $this->buildClientFromKey($key);

    return new KlaviyoListRepository($api_client);
  }

  /**
   * {@inheritdoc}
   */
  public function buildProfileRepositoryFromKeyId(
                            string $key_id): KlaviyoProfileRepositoryInterface {
    $api_client = $this->buildClientFromKeyId($key_id);

    return new KlaviyoProfileRepository($api_client);
  }

  /**
   * {@inheritdoc}
   */
  public function buildProfileRepositoryFromKey(
                        KeyInterface $key): KlaviyoProfileRepositoryInterface {
    $api_client = $this->buildClientFromKey($key);

    return new KlaviyoProfileRepository($api_client);
  }

  /**
   * Gets the factory for building Klaviyo API clients.
   *
   * @return \Drupal\klaviyo_api\KlaviyoClientFactoryInterface
   *   The API client factory.
   */
  protected function getApiFactory(): KlaviyoClientFactoryInterface {
    return $this->apiFactory;
  }

  /**
   * Gets an API client that connects using the specified key.
   *
   * @param string $key_id
   *   The machine name of the Key entity containing the credentials to connect
   *   to Klaviyo. Must be a Key of type "klaviyo_api".
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   */
  protected function buildClientFromKeyId(string $key_id): Klaviyo {
    return $this->getApiFactory()->buildClientFromKeyId($key_id);
  }

  /**
   * Gets an API client that connects using the specified key.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The API key the client should use for connecting. The key must be of
   *   type "klaviyo_api".
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   */
  protected function buildClientFromKey(KeyInterface $key): Klaviyo {
    return $this->getApiFactory()->buildClientFromKey($key);
  }

}

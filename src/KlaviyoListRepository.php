<?php

namespace Drupal\klaviyo_api;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\user\UserInterface;
use Klaviyo\Klaviyo;
use Klaviyo\Model\ProfileModel;

/**
 * The default service for locating Klaviyo lists and updating list membership.
 */
class KlaviyoListRepository implements KlaviyoListRepositoryInterface {

  /**
   * The interface for invoking alter hooks on modules.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Klaviyo API client.
   *
   * @var \Klaviyo\Klaviyo
   */
  protected $apiClient;

  /**
   * Constructor for KlaviyoListRepository.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The interface for invoking alter hooks on modules.
   * @param \Klaviyo\Klaviyo $api_client
   *   The Klaviyo API client.
   */
  public function __construct(ModuleHandlerInterface $module_handler,
                              Klaviyo $api_client) {
    $this->moduleHandler = $module_handler;
    $this->apiClient     = $api_client;
  }

  /**
   * {@inheritdoc}
   */
  public function getListNames(): array {
    $api_client = $this->getApiClient();

    // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
    /** @noinspection PhpUndefinedFieldInspection */
    $lists = $api_client->lists->getLists();

    $options = array_reduce(
      $lists,
      function ($options, $current_list) {
        $list_id   = $current_list['list_id'] ?? '';
        $list_name = $current_list['list_name'] ?? '';

        if (!empty($list_id) && !empty($list_name)) {
          $options[$list_id] = $list_name;
        }

        return $options;
      },
      []
    );

    asort($options);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function addUserToList(UserInterface $user, string $list_id): void {
    $email = $user->getEmail();

    if (empty($email)) {
      throw new \InvalidArgumentException(
        sprintf('User %d has no email address.', $user->id())
      );
    }

    $profile_data = ['$email' => $email];

    $this->getModuleHandler()->alter(
      'klaviyo_user_profile',
      $profile_data,
      $user
    );

    $klaviyo_client = $this->getApiClient();
    $profile_model  = new ProfileModel($profile_data);

    // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
    /** @noinspection PhpUndefinedFieldInspection */
    $klaviyo_client->lists->addMembersToList($list_id, [$profile_model]);
  }

  /**
   * Gets the interface for invoking alter hooks on modules.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler interface.
   */
  protected function getModuleHandler(): ModuleHandlerInterface {
    return $this->moduleHandler;
  }

  /**
   * Gets the Klaviyo API client.
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   */
  protected function getApiClient(): Klaviyo {
    return $this->apiClient;
  }

}

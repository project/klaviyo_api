<?php

namespace Drupal\klaviyo_api;

use Drupal\user\UserInterface;

/**
 * An interface for locating Klaviyo lists and updating list membership.
 */
interface KlaviyoListRepositoryInterface {

  /**
   * Requests all of the lists that are defined in the remote Klaviyo instance.
   *
   * The result is sorted in alphabetical order by the human-friendly names of
   * the lists.
   *
   * @return string[]
   *   An associative array in which each key is the ID of the list in the
   *   Klaviyo system and the value is the human-friendly name of that list.
   *
   * @throws \Klaviyo\Exception\KlaviyoException
   *   If the request fails.
   */
  public function getListNames(): array;

  /**
   * Adds the given user to the specified Klaviyo list.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user who will be added to the list.
   * @param string $list_id
   *   The ID of the list on the Klaviyo side.
   *
   * @throws \Klaviyo\Exception\KlaviyoException
   *   If the request fails.
   */
  public function addUserToList(UserInterface $user, string $list_id): void;

}

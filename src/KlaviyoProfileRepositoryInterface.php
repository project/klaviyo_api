<?php

namespace Drupal\klaviyo_api;

use Drupal\user\UserInterface;
use Klaviyo\Model\ProfileModel;

/**
 * An interface for locating and manipulating user profiles in Klaviyo.
 */
interface KlaviyoProfileRepositoryInterface {

  /**
   * Attempts to locate a Klaviyo user profile by email address.
   *
   * @param string $email
   *   The email address by which to locate the profile.
   *
   * @return \Klaviyo\Model\ProfileModel|null
   *   Either the profile; or, NULL if there is no such profile.
   *
   * @throws \Klaviyo\Exception\KlaviyoException
   *   If the request fails.
   */
  public function findProfileByEmail(string $email): ?ProfileModel;

  /**
   * Attempts to find the ID of a Klaviyo user profile by email address.
   *
   * @param string $email
   *   The email address by which to locate the profile.
   *
   * @return string|null
   *   Either the ID of the profile; or, NULL if there is no such profile.
   */
  public function findProfileIdByEmail(string $email): ?string;

  /**
   * Attempts to update the information Klaviyo has for a user profile.
   *
   * If there is no profile in Klaviyo with the email address specified for
   * the "old" email address, this method has no effect.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user for which an email update is being made.
   * @param string $old_email
   *   The email address that may have previously existed in Klaviyo for the
   *   user.
   * @param string $new_email
   *   The new email address for the user in Klaviyo.
   *
   * @return bool
   *   TRUE if a profile for the user was located in Klaviyo and successfully
   *   updated; or, FALSE if the profile could not be found or updated.
   *
   * @throws \Klaviyo\Exception\KlaviyoException
   *   If the request fails.
   */
  public function updateUserProfile(UserInterface $user,
                                    string $old_email,
                                    string $new_email): bool;

}

<?php

namespace Drupal\klaviyo_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigValueException;
use Drupal\key\Entity\Key;
use Drupal\key\KeyInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\klaviyo_api\Plugin\KeyType\KlaviyoApiKeyType;
use Klaviyo\Klaviyo;

/**
 * The default Klaviyo client factory.
 *
 * @noinspection PhpUnused
 */
class KlaviyoClientFactory implements KlaviyoClientFactoryInterface {

  /**
   * The factory for obtaining configuration settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The repository of secure keys.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Constructor for KlaviyoClientFactory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for obtaining configuration settings.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The repository of secure keys.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              KeyRepositoryInterface $key_repository) {
    $this->configFactory = $config_factory;
    $this->keyRepository = $key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function buildDefaultClient(): Klaviyo {
    $default_key_id = $this->getDefaultKeyId();

    return $this->buildClientFromKeyId($default_key_id);
  }

  /**
   * {@inheritdoc}
   */
  public function buildClientFromKeyId(string $key_id): Klaviyo {
    $key = $this->getKey($key_id);

    return $this->buildClientFromKey($key);
  }

  /**
   * {@inheritdoc}
   */
  public function buildClientFromKey(KeyInterface $key): Klaviyo {
    $key_type = $key->getKeyType();
    $key_id   = $key->id();

    if (!($key_type instanceof KlaviyoApiKeyType)) {
      throw new ConfigValueException(
        sprintf(
          "The provided key (%s) does not contain Klaviyo API credentials. A 'klaviyo_api' key type was expected.",
          $key_id
        )
      );
    }

    $key_config = $key_type->unserialize($key->getKeyValue());

    $private_key = $key_config[Constants::KEY_VALUE_PRIVATE_KEY];
    $public_key  = $key_config[Constants::KEY_VALUE_PUBLIC_KEY];

    if (empty($private_key)) {
      throw new ConfigValueException(
        sprintf(
          "There is no 'private key' value defined in the provided Klaviyo API key (%s).",
          $key_id
        )
      );
    }

    if (empty($public_key)) {
      throw new ConfigValueException(
        sprintf(
          "There is no 'public key' value defined in the provided Klaviyo API key (%s).",
          $key_id
        )
      );
    }

    return new Klaviyo($private_key, $public_key);
  }

  /**
   * Gets the configuration factory.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The factory for obtaining configuration settings.
   */
  protected function getConfigFactory(): ConfigFactoryInterface {
    return $this->configFactory;
  }

  /**
   * Gets the key repository.
   *
   * @return \Drupal\key\KeyRepositoryInterface
   *   The repository of secure keys.
   */
  protected function getKeyRepository(): KeyRepositoryInterface {
    return $this->keyRepository;
  }

  /**
   * Gets the ID of the key that contains the system-wide default API key.
   *
   * @return string
   *   The ID of the default key to use for Klaviyo API clients.
   */
  protected function getDefaultKeyId(): string {
    $config_id     = Constants::MODULE_CONFIG_ID;
    $config_key_id = Constants::CONFIG_KEY_DEFAULT_KEY_ID;

    $config         = $this->getConfigFactory()->get($config_id);
    $default_key_id = $config->get($config_key_id) ?? NULL;

    if (empty($default_key_id)) {
      throw new ConfigValueException(
        sprintf(
          'A default key for Klaviyo (%s.%s) has not been configured.',
          $config_id,
          $config_key_id
        )
      );
    }

    return $default_key_id;
  }

  /**
   * Gets the Klaviyo API key that has the specified ID.
   *
   * @return \Drupal\key\Entity\Key
   *   The pre-shared key.
   */
  protected function getKey($key_id): Key {
    $key = $this->getKeyRepository()->getKey($key_id);

    if (empty($key)) {
      throw new \InvalidArgumentException(
        sprintf(
          "Could not locate the specified key ('%s'). Please specify a different key.",
          $key_id
        )
      );
    }

    return $key;
  }

}

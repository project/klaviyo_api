<?php

namespace Drupal\klaviyo_api;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\user\UserInterface;
use Klaviyo\Exception\KlaviyoApiException;
use Klaviyo\Klaviyo;
use Klaviyo\Model\ProfileModel;

/**
 * The default service for locating and manipulating user profiles in Klaviyo.
 */
class KlaviyoProfileRepository implements KlaviyoProfileRepositoryInterface {

  /**
   * The interface for invoking alter hooks on modules.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Klaviyo API client.
   *
   * @var \Klaviyo\Klaviyo
   */
  protected $apiClient;

  /**
   * Constructor for KlaviyoProfileRepository.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The interface for invoking alter hooks on modules.
   * @param \Klaviyo\Klaviyo $api_client
   *   The Klaviyo API client.
   */
  public function __construct(ModuleHandlerInterface $module_handler,
                              Klaviyo $api_client) {
    $this->moduleHandler = $module_handler;
    $this->apiClient     = $api_client;
  }

  /**
   * {@inheritdoc}
   */
  public function findProfileByEmail(string $email): ?ProfileModel {
    $profile_id = $this->findProfileIdByEmail($email);

    if (empty($profile_id)) {
      $profile = NULL;
    }
    else {
      $client = $this->getApiClient();

      try {
        // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
        /** @noinspection PhpUndefinedFieldInspection */
        $profile = $client->profiles->getProfile($profile_id);
      }
      catch (KlaviyoApiException $ex) {
        // This non-fatal exception indicates
        // "There is no profile matching the given parameters".
        $profile = NULL;
      }
    }

    return $profile;
  }

  /**
   * {@inheritdoc}
   */
  public function findProfileIdByEmail(string $email): ?string {
    $client = $this->getApiClient();

    try {
      // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
      /** @noinspection PhpUndefinedFieldInspection */
      $result     = $client->profiles->getProfileIdByEmail($email);
      $profile_id = $result['id'] ?? NULL;
    }
    catch (KlaviyoApiException $ex) {
      // This exception indicates
      // "There is no profile matching the given parameters".
      $profile_id = NULL;
    }

    return $profile_id;
  }

  /**
   * {@inheritdoc}
   */
  public function updateUserProfile(UserInterface $user,
                                    string $old_email,
                                    string $new_email): bool {
    // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
    /** @noinspection PhpUndefinedFieldInspection */
    $profile_id = $this->findProfileIdByEmail($old_email);

    if ($profile_id === NULL) {
      $updated = FALSE;
    }
    else {
      $profile_data = ['$email' => $new_email];

      $this->getModuleHandler()->alter(
        'klaviyo_user_profile',
        $profile_data,
        $user
      );

      $client = $this->getApiClient();

      // phpcs:ignore Drupal.Commenting.InlineComment.DocBlock
      /** @noinspection PhpUndefinedFieldInspection */
      $client->profiles->updateProfile($profile_id, $profile_data);

      $updated = TRUE;
    }

    return $updated;
  }

  /**
   * Gets the interface for invoking alter hooks on modules.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler interface.
   */
  protected function getModuleHandler(): ModuleHandlerInterface {
    return $this->moduleHandler;
  }

  /**
   * Gets the Klaviyo API client.
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   */
  protected function getApiClient(): Klaviyo {
    return $this->apiClient;
  }

}

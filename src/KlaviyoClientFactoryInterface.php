<?php

namespace Drupal\klaviyo_api;

use Drupal\key\KeyInterface;
use Klaviyo\Klaviyo;

/**
 * An interface for obtaining API clients that can communicate with Klaviyo.
 */
interface KlaviyoClientFactoryInterface {

  /**
   * Builds a Klaviyo API client instance using a system-default key.
   *
   * A default API key must have been configured on the site.
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildDefaultClient(): Klaviyo;

  /**
   * Builds a Klaviyo API client instance using the Key with the specified ID.
   *
   * @param string $key_id
   *   The ID of the key containing the API credentials.
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildClientFromKeyId(string $key_id): Klaviyo;

  /**
   * Builds a Klaviyo API client instance using the specified Key.
   *
   * @param \Drupal\key\KeyInterface $key
   *   The key containing the API credentials.
   *
   * @return \Klaviyo\Klaviyo
   *   The API client.
   *
   * @throws \Drupal\Core\Config\ConfigValueException
   *   If either a default API key is not configured or is not the correct type
   *   of key.
   */
  public function buildClientFromKey(KeyInterface $key): Klaviyo;

}

<?php

namespace Drupal\klaviyo_api\Element;

use Drupal\klaviyo_api\Constants;
use Drupal\klaviyo_api\KlaviyoListRepositoryInterface;
use Drupal\klaviyo_api\KlaviyoRepositoryFactoryInterface;
use Klaviyo\Exception\KlaviyoException;

/**
 * A trait for form elements to be able to retrieve Klaviyo list information.
 */
trait KlaviyoOptionsListTrait {

  /**
   * Gets a list repository that will use the given API key for requests.
   *
   * @param string|null $api_key_id
   *   The machine ID of the API key to use when connecting to Klaviyo. If
   *   omitted, the default list repository is returned.
   *
   * @return \Drupal\klaviyo_api\KlaviyoListRepositoryInterface
   *   The list repository.
   */
  protected static function getListRepository(
                          ?string $api_key_id): KlaviyoListRepositoryInterface {
    if (empty($api_key_id)) {
      $list_repository = \Drupal::service('klaviyo_api.list.repository');
    }
    else {
      $repository_factory = \Drupal::service('klaviyo_api.repository_factory');
      assert($repository_factory instanceof KlaviyoRepositoryFactoryInterface);

      $list_repository =
        $repository_factory->buildListRepositoryFromKeyId($api_key_id);
    }

    return $list_repository;
  }

  /**
   * Fetches the options for what lists are available in Klaviyo.
   *
   * @param \Drupal\klaviyo_api\KlaviyoListRepositoryInterface $list_repository
   *   The repository from which to query for lists.
   *
   * @return string[]
   *   An associative array in which each key is the ID of the list in the
   *   Klaviyo system and the value is the human-friendly name of that list.
   */
  protected static function fetchOptions(
    KlaviyoListRepositoryInterface $list_repository): array {
    try {
      $options = $list_repository->getListNames();
    }
    catch (KlaviyoException $ex) {
      \Drupal::messenger()->addError(
        'Could not request Klaviyo lists: ' . $ex->getMessage()
      );

      \watchdog_exception(
        Constants::MODULE_NAME,
        $ex,
        'Could not request Klaviyo lists: %type: @message in %function (line %line of %file).'
      );

      $options = [];
    }

    return $options;
  }

}

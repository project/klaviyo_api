<?php

namespace Drupal\klaviyo_api\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Provides a select form element that displays available Klaviyo lists.
 *
 * Properties:
 * - #api_key_id: (optional) The machine ID of the Key entity containing the API
 *   key that will be used to fetch what Klaviyo lists are available. If not
 *   provided, the site-default API key will be used instead.
 * - #empty_option: (optional) The label to show for the first default option.
 *   By default, the label is automatically set to "- Select -" for a required
 *   field and "- None -" for an optional field.
 * - #empty_value: (optional) The value for the first default option, which is
 *   used to determine whether the user submitted a value or not.
 *   - If #required is TRUE, this defaults to '' (an empty string).
 *   - If #required is not TRUE and this value isn't set, then no extra option
 *     is added to the select control, leaving the control in a slightly
 *     illogical state, because there's no way for the user to select nothing,
 *     since all user agents automatically preselect the first available
 *     option. But people are used to this being the behavior of select
 *     controls.
 *   - If #required is not TRUE and this value is set (most commonly to an
 *     empty string), then an extra option (see #empty_option above)
 *     representing a "non-selection" is added with this as its value.
 * - #multiple: (optional) Indicates whether one or more options can be
 *   selected. Defaults to FALSE.
 * - #default_value: Must be NULL or not set in case there is no value for the
 *   element yet, in which case a first default option is inserted by default.
 *   Whether this first option is a valid option depends on whether the field
 *   is #required or not.
 * - #required: (optional) Whether the user needs to select an option (TRUE)
 *   or not (FALSE). Defaults to FALSE.
 * - #size: The number of rows in the list that should be visible at one time.
 *
 * Usage example:
 * @code
 * $form['example_list_select'] = [
 *   '#type' => 'klaviyo_list_select',
 *   '#title' => $this->t('Select a list'),
 * ];
 * @endcode
 *
 * @FormElement("klaviyo_list_select")
 * @noinspection PhpUnused
 */
class KlaviyoListSelector extends Select {

  use KlaviyoOptionsListTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $info  = parent::getInfo();
    $class = get_class($this);

    // Add a process function.
    array_unshift(
      $info['#process'],
      [$class, 'processListSelect']
    );

    return $info;
  }

  /**
   * Populates the render array for the Klaviyo list select form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @noinspection PhpUnusedParameterInspection
   */
  public static function processListSelect(array &$element,
                                           FormStateInterface $form_state,
                                           array &$complete_form): array {
    $key_id = $element['#api_key_id'] ?? NULL;

    $list_repository = self::getListRepository($key_id);
    $options         = self::fetchOptions($list_repository);

    $element['#options'] = $options;

    // Do not display a 'multiple' select box if there is only one option.
    if ($element['#multiple'] && (count($options) <= 1)) {
      $element['#multiple'] = FALSE;
    }

    self::filterDefaultValue($element['#default_value'], $options);

    return $element;
  }

  /**
   * Filters the options selected by by what's available in Klaviyo remotely.
   *
   * The default value can be either an array (for multiple-select controls) or
   * a scalar (for single-select controls). The value will be converted into an
   * array as part of processing.
   *
   * @param array|string $default_value
   *   A reference to the '#default_value' of the field. The contents of this
   *   field will be modified by this call.
   * @param array $options
   *   An associative array of the lists available in Klaviyo, where each value
   *   is the human-friendly name of the list and the key is the ID of the list.
   */
  protected static function filterDefaultValue(&$default_value,
                                               array $options): void {
    if (!is_array($default_value)) {
      $default_value = [$default_value];
    }

    // Keep each value that it actually is in the list of options.
    $default_value =
      array_filter(
        $default_value,
        function ($value) use ($options) {
          return isset($options[$value]);
        }
      );
  }

}

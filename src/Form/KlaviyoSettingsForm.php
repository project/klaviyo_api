<?php

namespace Drupal\klaviyo_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\klaviyo_api\Constants;

/**
 * Configure settings that affect Klaviyo API integration site-wide.
 *
 * @noinspection PhpUnused
 */
class KlaviyoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'klaviyo_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [Constants::MODULE_CONFIG_ID];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,
                            FormStateInterface $form_state): array {
    $config = $this->config(Constants::MODULE_CONFIG_ID);

    $form['help'] = [
      '#markup' => $this->t(
        'The authentication settings provided here are used whenever an instance of the system-wide "default" Klaviyo API client is requested. This does not prohibit other parts of the system from storing, managing, and using their own use-case-specific API keys.'
      ),
    ];

    $form[Constants::CONFIG_KEY_DEFAULT_KEY_ID] = [
      '#type'          => 'key_select',
      '#title'         => $this->t('Key containing Klaviyo credentials'),
      '#empty_value'   => NULL,
      '#empty_option'  => $this->t('- None -'),
      '#key_filters'   => ['type' => 'klaviyo_api'],
      '#required'      => TRUE,
      '#default_value' => $config->get(Constants::CONFIG_KEY_DEFAULT_KEY_ID),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state_values = $form_state->getValues();

    $key_id = $form_state_values[Constants::CONFIG_KEY_DEFAULT_KEY_ID];

    $config = $this->config(Constants::MODULE_CONFIG_ID);
    $config->set(Constants::CONFIG_KEY_DEFAULT_KEY_ID, $key_id);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}

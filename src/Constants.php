<?php

namespace Drupal\klaviyo_api;

/**
 * A class containing all constants used in multiple places of this module.
 *
 * Where possible, constants specific to an interface or specific classes should
 * live in those interfaces and classes instead of this class.
 */
abstract class Constants {

  /**
   * The machine name for this module.
   */
  const MODULE_NAME = 'klaviyo_api';

  /**
   * The machine name of the config for this module.
   */
  const MODULE_CONFIG_ID = 'klaviyo_api.settings';

  /**
   * The name of the key in the module config holding the PSK key ID.
   */
  const CONFIG_KEY_DEFAULT_KEY_ID = 'default_key_id';

  /**
   * The name of the value in Klaviyo API key types containing the private key.
   */
  const KEY_VALUE_PRIVATE_KEY = 'private_key';

  /**
   * The name of the value in Klaviyo API key types containing the public key.
   */
  const KEY_VALUE_PUBLIC_KEY = 'public_key';

}

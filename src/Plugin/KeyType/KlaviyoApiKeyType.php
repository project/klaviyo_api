<?php

namespace Drupal\klaviyo_api\Plugin\KeyType;

use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key that stores Klavyio connection information.
 *
 * The underlying key is serialized into JSON format.
 *
 * @KeyType(
 *   id = "klaviyo_api",
 *   label = @Translation("Klaviyo API Key"),
 *   description = @Translation("Stores a public and private key required to authenticate with Klaviyo."),
 *   group = "connection_string",
 *   key_value = {
 *     "plugin" = "klaviyo_api"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "public_key" = {
 *         "label" = @Translation("Public API key"),
 *         "required" = true
 *       },
 *       "private_key" = {
 *         "label" = @Translation("Private API key"),
 *         "required" = true
 *       },
 *     }
 *   }
 * )
 */
class KlaviyoApiKeyType extends AuthenticationMultivalueKeyType {

}

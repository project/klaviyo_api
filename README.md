# Klaviyo API
A lightweight wrapper around the
[Klaviyo PHP library](https://github.com/klaviyo/php-klaviyo), pulled-in via
Composer, for Drupal 8.x.

## Use Cases
This module provides:

1. An API for other modules to easily obtain Klaviyo API clients using service
   injection, either using a site-wide "default" API key, or any number of
   site-custom API keys (e.g. to support connecting to multiple Klaviyo
   accounts).
2. Repository service objects that wrap the Klaviyo PHP API to make it more
   convenient and faster to accomplish common tasks, like creating, retrieving,
   and updating user profiles and adding users to lists.
3. A sub-module for automatically adding new users to a Klaviyo list and
   updating the email addresses of existing users within Klaviyo whenever their
   email addresses are updated in Drupal.
4. A sub-module for referencing Klaviyo lists from fields in entities.

## API Key Storage
This module depends on the Key module to store and manage API keys; this module
provides a custom Key type and Key input to make entry of Klaviyo public and
private keys easy. This allows API keys to be stored using any available
provider that Key supports, including environment variables, configuration,
Lockr, etc.

## Usage
### Installation
It is recommended to install this via Composer so that the required version of
the Klaviyo PHP client can be pulled in.

```sh
composer require 'drupal/klaviyo_api'
```

### Getting the Default Klaviyo API Client
After installation, in order for there to be a default API client available,
you must either:
- Edit the "Default Klaviyo API Key" under:
  "Configuration" -> "System" -> "Keys"
  (`/admin/config/system/keys/manage/klaviyo_default_key`);
  OR
- Define a new key of type "Klaviyo API Key" under
  "Configuration" -> "System" -> "Keys", and then set the site to use the new
  key as the default API key under:
  "Configuration" -> "Web services" -> "Klaviyo"
  (`/admin/config/services/klaviyo`).

Afterwards, you can obtain a default API client by getting the
`klaviyo_api.client` service and then calling any of the methods on the object
you get back that are
[documented for the PHP library](https://github.com/klaviyo/php-klaviyo#api-examples).
For example, in procedural code and hooks:
```php
$client = \Drupal::service('klaviyo_api.client');
$metrics = $client->metrics->getMetrics();
```

### Getting a Klaviyo API Client with a Custom Key
If your site needs to work with multiple Klaviyo deployments, you do not need
to configure the default key. Instead, define custom keys (of type
"Klaviyo API Key") for each of the different deployments, obtain the Klaviyo
API factory service, and construct a different API client for each custom key.

For example:
```php
$client_factory = \Drupal::service('klaviyo_api.client.factory');
$client = $client_factory->buildClientFromKeyId('my_klaviyo_key');
$metrics = $client->metrics->getMetrics();
```
